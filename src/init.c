#include "init.h"

int open_port(void)
{
	int fd;
	if ((fd = open("/dev/ttyS0",O_RDWR|O_NOCTTY|O_NDELAY)) == -1)
	{
		perror("open_port: Unable to open device\n");
		exit (2);
	}
	fcntl(fd,F_SETFL,0);
	return fd;
}

int configure_port(int fd)
{
	struct termios port_settings;
	/* set port speed to 1200 bauds */
	cfsetospeed(&port_settings,B1200);
	cfsetispeed(&port_settings,B1200);
	//cfsetspeed(&port_settings,B1200);
	/* set parity flags */
	port_settings.c_cflag |= PARENB;
	/* stop bit */
	port_settings.c_cflag &= ~CSTOPB;
	/* 7 bit */
	port_settings.c_cflag &= ~CSIZE;
	port_settings.c_cflag |= CS7;
	port_settings.c_cflag &= ~CLOCAL;
	/* unset control bit */ 
	port_settings.c_cflag &= ~CRTSCTS;
	port_settings.c_cflag |= CREAD;
	/* raw output */
	port_settings.c_oflag &= ~OPOST;
	tcsetattr(fd,TCSANOW,&port_settings);
//	tcflow(fd,TCION);
	
	return fd;
}
