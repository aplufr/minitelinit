#include "init.h"
#include "../config.h"

#define BYTETOBINARY(byte)  \
	(byte & 0x80 ? 1 : 0), \
	(byte & 0x40 ? 1 : 0), \
	(byte & 0x20 ? 1 : 0), \
	(byte & 0x10 ? 1 : 0), \
	(byte & 0x08 ? 1 : 0), \
	(byte & 0x04 ? 1 : 0), \
	(byte & 0x02 ? 1 : 0), \
	(byte & 0x01 ? 1 : 0) 


int main (int argc, char * argv)
{
        int fd;
	FILE * minitel;
	char str;
	char  str2[30];
	int i,j;
	printf("%s version %s\n",PACKAGE_NAME,PACKAGE_VERSION);

	/* init port */
	fd = open_port();
	fd = configure_port(fd);
	minitel = fdopen(fd,"rb+");

	printf("Port ready\n");

	/* clean screen */
	write(fd,"\f",1);

	fprintf(minitel,"Hello world.\r\n");
	fprintf(minitel,"With this program we can check both-side of communication\r\n");
	fprintf(minitel,"Please press any key on the minitel\r\n");
	i = read(fd,&str,1);
	/* printf("@%d::%X -- %d%d%d%d% d%d%d%d (%c)\n",i,str, BYTETOBINARY(str),str); */
	if (i) 
		fprintf(minitel,"You pressed key '%c' (hex:%02X)\r\n",str,str);
	else
		fprintf(minitel,"Read ended to early, can be an error\r\n");
	fprintf(minitel,"\r\n");

	fprintf(minitel,"  mm   mmmm  m     m    m   mmmmm mmmmm\r\n");
	fprintf(minitel,"  ##   #  \"# #     #    #   #     #   \"#\r\n");
	fprintf(minitel," #  #  #mm#\" #     #    #   #mmmm #mmmm'\r\n");
	fprintf(minitel," #mm#  #     #     #    #   #     #   \"m\r\n");
	fprintf(minitel,"#    # #     #mmmm \"mmmm\" # #     #    \"\r\n");


	return 0;
}

